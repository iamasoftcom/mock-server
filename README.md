# Handlers

## Handlers list

```
onProxyReq: (proxyReq, req, res) => {
// Before requesting proxy
},
onProxyRes: (proxyRes, req, res) => {
// After requesting proxy
},
transformJson: async (data, req, res) => {
// Transform raw data
},
transformHtml: async ($, req, res) => {
// Transform when content type is HTML
},
transformJson: async (data, req, res) => {
// Transform when content type is JSON
},
updateRequestInput: requestInput => {
// Modify requestInput (body, pathParams, query)
},
middleware: (req, res, next) => {
// Express middleware
}
```

## Middleware example

````
middleware: (req, res, next) => {
  const { pathParams } = req;
  if (!pathParams.codeTiers || !pathParams.query) {
    res.status(400).end();
  }
  res.status(200).send(
    Array.from({ length: 10 }).map(() => {
      return {
        nom: faker.name.lastName(),
        prenom: faker.name.firstName(),
        email: faker.internet.email(),
        tel: faker.phone.phoneNumber()
      };
    })
  );
}
```

3000 -> non-bancaire
3001 -> parametrage
3002 -> cas-mock
3003 -> admin ui

/data/ : mocks
/data/state.json : server state : {
selectedSession
path conf (mock, record, )
}

config/default.yml :

handlers/

Multiple recording session

/data/conf.yml => conf des proxys
/data/state.json => state (tmp ?)
/data/4000/session_id/routeId/res-1.json
/data/4000/handlers.js
/data/4000/swagger.yml
/data/4000/local.yml
````
