const path = require("path");
const fs = require("fs-extra");
const Promise = require("bluebird");
const _ = require("lodash");
const hash = require("object-hash");
var colors = require("colors/safe");
const { getHandlersByRouteId } = require("./utils");

module.exports = class RecordSession {
  constructor(name, dataPath, handlers) {
    this.name = _.snakeCase(name);
    this.state = null;
    this.datas = {};
    this.routesIndex = {};
    this.dataPath = path.join(dataPath, _.snakeCase(this.name));
    this.handlers = handlers;
  }

  async startRecording() {
    if (this.name === "default") {
      throw new Error("Cannot record for the default session");
    }
    await fs.remove(this.dataPath);
    await fs.ensureDir(this.dataPath);
    this.state = "recording";
  }

  async startPlaying() {
    await fs.ensureDir(this.dataPath);
    try {
      await fs.readdir(this.dataPath).then(directories => {
        return Promise.map(directories, routeId => {
          const directoryPath = path.join(this.dataPath, routeId);
          return fs.readdir(directoryPath).then(files => {
            return Promise.map(files.sort(), resFile => {
              return fs
                .readJson(path.join(directoryPath, resFile))
                .then(data => {
                  const dashIndex = resFile.lastIndexOf("-");
                  const dotIndex = resFile.lastIndexOf(".");
                  console.log(directoryPath, resFile);
                  const fileId = resFile.substring(
                    0,
                    (dashIndex >= 0 && dashIndex) || (dotIndex >= 0 && dotIndex)
                  );
                  const dataRequestInput = {
                    files: {},
                    ...data.input
                  };
                  this.addNewRecord(routeId, dataRequestInput, fileId);
                });
            });
          });
        });
      });
      this.state = "playing";
      console.info(
        colors.bgMagenta(
          `[${this.dataPath}] START_PLAY_SESSION ${this.name}: SUCCESS`
        )
      );
    } catch (e) {
      console.error(colors.red(`START_PLAY_SESSION ${this.name}: ERROR`));
      throw e;
    }
  }

  getPlayRequestMockPath(req) {
    const requestInput = this.getRequestInput(req);
    const requestId = this.getRequestId(req.routeId, requestInput);
    if (!this.datas[requestId]) {
      console.log(`could not find mock for ${req.routeId}`, requestInput);
      return null;
    }

    const data = this.datas[requestId];
    // const fileId = requestId.substr(0, 8);
    // FIXME : how to manager sequentials responses ?
    // if (data.playIndex + 1 > data.recordIndex) {
    //   return null;
    // }
    // data.playIndex++;

    const fileName = `${data.fileId}-${data.playIndex}.json`;
    const filePath = path.join(data.resDir, fileName);

    return filePath;
  }

  addNewRecord(routeId, requestInputData, fileId) {
    const handlers = getHandlersByRouteId(this.handlers, routeId);
    if (!handlers) {
      throw new Error(`Swagger not found for route : ${routeId}`);
    }
    let filteredInputData = requestInputData;
    if (handlers && handlers.updateRequestInput) {
      filteredInputData = handlers.updateRequestInput(requestInputData);
    }
    const requestId = this.getRequestId(routeId, filteredInputData);
    if (!this.datas[requestId]) {
      // create a new file

      this.datas[requestId] = {
        resDir: this.getRouteDataDir(routeId),
        fileId:
          fileId || this.getRequestId(routeId, requestInputData).substr(0, 8),
        recordIndex: 1,
        playIndex: 1
      };
    }

    const data = this.datas[requestId];
    // FIXME : how to manager sequentials responses ?
    // data.recordIndex++;
    return data;
  }

  getRecordRequestMockPath(req) {
    const data = this.addNewRecord(req.routeId, this.getRequestInput(req));

    const fileName = `${data.fileId}-${data.recordIndex}.json`;
    const filePath = path.join(data.resDir, fileName);

    return filePath;
  }

  getRouteDataDir(routeId) {
    return path.join(this.dataPath, routeId);
  }

  getRequestId(routeId, requestInputData) {
    const requestObj = {
      ...requestInputData,
      routeId
    };
    // JSON writing will remove some attributes (undefined and null) this ensure
    // that we calculate the same hash each time.
    return hash(JSON.parse(JSON.stringify(requestObj)));
  }

  async getMockResponse(req) {
    const filePath = this.getPlayRequestMockPath(req);
    if (!filePath) {
      return null;
    }
    return fs.readJSON(filePath);
  }

  getResponseHeaders(res) {
    return {
      location: res._headers["location"],
      "content-type": res._headers["content-type"]
    };
  }

  getRequestInput(req) {
    if (req.handlers && req.handlers.updateRequestInput) {
      return req.handlers.updateRequestInput(req.requestInput);
    } else {
      return req.requestInput;
    }
  }

  async recordApiResponse(data, req, res) {
    if (this.state === "recording" && res.servedBy !== "mock") {
      const contentType = res._headers["content-type"];
      const headers = this.getResponseHeaders(res);
      const statusCode = res.statusCode;

      let objData;
      if (contentType && contentType === "application/json") {
        objData = JSON.parse(data);
      } else if (contentType && contentType.includes("text/javascript")) {
        objData = data.toString();
      } else if (contentType && contentType.includes("text/html")) {
        objData = data.toString();
      } else {
        objData = data.toString();
      }

      const input = this.getRequestInput(req);

      const resData = {
        input,
        headers,
        statusCode,
        data: objData
      };

      const filePath = this.getRecordRequestMockPath(req);
      await fs.ensureDir(path.dirname(filePath));
      await fs.writeJSON(filePath, resData, { spaces: 2 });
      res.recorded = filePath;
    }
  }
};
