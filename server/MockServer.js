const EventEmitter = require("events");
const express = require("express");
const _ = require("lodash");
const Promise = require("bluebird");
const fs = require("fs-extra");
const swaggerMiddleware = require("swagger-express-middleware");
const httpProxyMiddleware = require("http-proxy-middleware");
const transformerProxy = require("transformer-proxy");
const path = require("path");
const RecordSession = require("./RecordSession");
const cheerio = require("cheerio");
var colors = require("colors/safe");
const { catchAll, getRouteId } = require("./utils");

const SwaggerParser = require("swagger-parser");
const parser = new SwaggerParser();

function backendProxyFactory(mockServer) {
  return httpProxyMiddleware({
    target: mockServer.targetUrl,
    changeOrigin: false,
    autoRewrite: true,
    logLevel: "warn",
    onProxyRes(proxyRes, req, res) {
      res.servedBy = "proxy";
      if (proxyRes.headers["location"]) {
        // Replace all urls
        mockServer.proxysConf.forEach(conf => {
          proxyRes.headers["location"] = proxyRes.headers["location"].replace(
            conf.targetUrl,
            `http://localhost:${conf.proxyPort}`
          );
        });
      }

      if (req.handlers && req.handlers.onProxyRes) {
        req.handlers.onProxyRes(proxyRes, req, res);
      }
      // console.log(proxyRes, req, res);
    },
    onProxyReq(proxyReq, req, res) {
      if (req.handlers && req.handlers.onProxyReq) {
        req.handlers.onProxyReq(proxyReq, req, res);
      }
      if (req.body && req.complete) {
        const bodyData = JSON.stringify(req.body);
        proxyReq.setHeader("Content-Type", "application/json");
        proxyReq.setHeader("Content-Length", Buffer.byteLength(bodyData));
        proxyReq.write(bodyData);
      }
    }
  });
}

function getTransformFunction(contentType, handlers, mockServer) {
  if (!handlers) {
    return null;
  }

  if (
    handlers.transformOn &&
    handlers.transformOn.indexOf(mockServer.mode) < 0
  ) {
    // transformOn to specify on which mode to operate
    return null;
  }

  let transformFn;
  if (
    contentType &&
    contentType === "application/json" &&
    handlers.transformJson
  ) {
    transformFn = async (data, req, res) => {
      const obj = JSON.parse(data);
      const newObj = await handlers.transformJson(obj, req, res);
      return JSON.stringify(newObj);
    };
  } else if (
    contentType &&
    contentType.includes("text/html") &&
    handlers.transformHtml
  ) {
    transformFn = async (data, req, res) => {
      const $ = cheerio.load(data);
      const new$ = await handlers.transformHtml($, req, res);
      return new$.html();
    };
  } else {
    transformFn = handlers.transform;
  }
  return transformFn;
}

async function transformResponse(data, req, res, mockServer) {
  const contentType = res._headers["content-type"];
  const transformFn = getTransformFunction(
    contentType,
    req.handlers,
    mockServer
  );
  if (transformFn) {
    req.modified = true;
    try {
      const newData = await transformFn(data, req, res);
      return newData;
    } catch (e) {
      console.error("Error while transforming response", e);
      return data;
    }
  } else {
    return data;
  }
}

function recordTransformProxyRequestFactory(mockServer) {
  const backendProxy = backendProxyFactory(mockServer);
  // transform, record and proxy the request
  return function proxyRequest(req, res, next) {
    const transformer = transformerProxy(async (data, req, res) => {
      try {
        mockServer.recordSession &&
          mockServer.recordSession.state === "recording" &&
          (await mockServer.recordSession.recordApiResponse(data, req, res));
      } catch (e) {
        console.error(e);
      }
      return transformResponse(data, req, res, mockServer);
    });
    transformer(req, res, () => {
      backendProxy(req, res, next);
    });
  };
}

// function setCustomHeaders(req, res) {
//   res.set("MOCKER-servedBy", res.servedBy);
//   res.set("MOCKER-response-modified", !!req.modified);
//   res.set("MOCKER-routeId", req.routeId);
//   res.set("MOCKER-recorded", !!res.recorded);
// }

async function expressServerFactory(mockServer) {
  // Load swagger defs
  const swaggerDefs = await parser.dereference(mockServer.handlers, {
    dereference: {
      circular: false
    },
    validate: {
      spec: true
    }
  });
  // Create enhanced proxy with recorder and transformer
  const proxyRequest = recordTransformProxyRequestFactory(mockServer);

  const router = express.Router();
  return new Promise((resolve, reject) => {
    swaggerMiddleware(swaggerDefs, router, function(err, middleware) {
      if (err) {
        reject(err);
        return;
      }

      router.use((req, res, next) => {
        req.start = Date.now();
        req.serverName = mockServer.serverName;
        res.on("finish", () => {
          req.stop = Date.now();
          mockServer.logRequest(req, res);
        });
        next();
      });

      if (mockServer.middlewares) {
        mockServer.middlewares.forEach(middlewareConf => {
          if (middlewareConf.type === "statics") {
            console.log(
              path.resolve(mockServer.dataPath, middlewareConf.staticsPath)
            );
            router.use(
              middlewareConf.path,
              express.static(
                path.resolve(mockServer.dataPath, middlewareConf.staticsPath),
                {
                  setHeaders: (res, path, stat) => {
                    res.servedBy = "statics";
                  }
                }
              )
            );
          } else if (middlewareConf.type === "proxy") {
            router.use(
              middlewareConf.path,
              httpProxyMiddleware({
                target: middlewareConf.targetUrl,
                changeOrigin: false,
                autoRewrite: true,
                logLevel: "warn",
                pathRewrite: {
                  "^/iam": "" // rewrite path
                }
              })
            );
          }
        });
      }

      router.use(
        middleware.metadata(),
        middleware.CORS(),
        middleware.files(),
        middleware.parseRequest()
        // middleware.validateRequest()
      );

      router.use((req, res, next) => {
        // console.log(req);
        req.routeId = getRouteId(req);
        req.handlers = mockServer.getHandlers(req);
        req.requestInput = {
          body: req.body || {},
          query: req.query || {},
          pathParams: req.pathParams || {},
          files: _.reduce(
            req.files || {},
            (res, file, fieldName) => {
              if (!file) {
                throw new Error(`Undefined ${fieldName}`);
              }
              res[fieldName] = {
                mimetype: file.mimetype,
                originalname: file.originalname
              };
              return res;
            },
            {}
          )
        };
        const delay = (req.handlers && req.handlers.delay) || 0;

        setTimeout(() => {
          next();
        }, delay);
      });

      router.use((req, res, next) => {
        const isPlaying =
          mockServer.recordSession &&
          mockServer.recordSession.state === "playing";

        if (isPlaying && req.handlers && req.handlers.middleware) {
          res.servedBy = "middleware";
          req.handlers.middleware(req, res, next);
        } else {
          next();
        }
      });

      router.use(
        catchAll(async (req, res, next) => {
          const isPlaying =
            mockServer.recordSession &&
            mockServer.recordSession.state === "playing";

          const obj =
            isPlaying && (await mockServer.recordSession.getMockResponse(req));

          if (obj) {
            // FIXME content should be in mock
            res.servedBy = "mock";
            _.forOwn(obj.headers, (val, key) => {
              res.set(key, val);
            });
            const data = await transformResponse(
              obj.data,
              req,
              res,
              mockServer
            );
            res.status(obj.statusCode).send(data);
          } else {
            next();
          }
        })
      );

      router.use((req, res, next) => {
        if (!mockServer.offline) {
          proxyRequest(req, res, next);
        } else {
          next(new Error("NO MOCK OR PROXY OFFLINE"));
        }
      });

      router.use((req, res, next) => {
        res.send(404).end();
      });

      router.use((err, req, res, next) => {
        console.error(err);
        res.status(500).send(err.message);
        // if (err.message.startsWith("Resource not found:")) {
        //   // Request will go there if NOT found in swagger
        //   // proxyRequest(req, res, next);
        //   throw new Error(err.message);
        // } else {
        //   console.error(err);
        //   res.status(500).send();
        // }
      });

      resolve(router);
    });
  });
}

const useWithFullPath = (app, path, middleware) => {
  app.use((req, res, next) => {
    if (req.path.startsWith(path)) {
      middleware(req, res, next);
    } else {
      next();
    }
  });
};

class MockRouter {
  constructor(conf, proxysConf, mockServer) {
    Object.assign(this, conf);
    this.proxysConf = proxysConf;
    this.mockServer = mockServer;
    try {
      this.handlers = require(path.join(this.dataPath, "handlers.js"));
    } catch (e) {
      throw new Error(e);
      // console.warn(`No handlers defined for ${this.serverName}`, e.message);
    }
  }

  async init() {
    await fs.ensureDir(this.dataPath);
    this.router = await expressServerFactory(this);
    useWithFullPath(this.mockServer.expressServer, this.path, this.router);
    // this.mockServer.expressServer.use(this.path, this.router);
  }

  async start(sessionName, mode, offline) {
    this.offline = offline;
    this.recordSession = new RecordSession(
      sessionName,
      path.join(this.dataPath, "sessions"),
      this.handlers
    );
    this.mode = mode;
    if (mode === "record") {
      await this.recordSession.startRecording();
    } else if (mode === "play") {
      await this.recordSession.startPlaying();
    }
  }

  async stop() {
    // TODO close session
    this.mode = "start";
    this.recordSession = null;
  }

  logRequest(req, res) {
    this.mockServer.logRequest(req, res);
  }

  getHandlers(req) {
    if (this.handlers) {
      const pathName = req.swagger && req.swagger.pathName;
      const swaggerRoute = this.handlers.paths[pathName];
      if (swaggerRoute) {
        const swaggerMethod = swaggerRoute[req.method.toLowerCase()];
        return swaggerMethod;
      }
    }
  }

  static async build(conf, proxysConf, mockServer) {
    const mockRouter = new MockRouter(conf, proxysConf, mockServer);
    await mockRouter.init();
    return mockRouter;
  }
}

class MockServer extends EventEmitter {
  constructor(conf, proxysConf) {
    super();
    Object.assign(this, conf);
    this.proxysConf = proxysConf;
  }

  async init() {
    this.expressServer = express();
    this.mockRouters = await Promise.mapSeries(this.routers, routerConf => {
      routerConf.dataPath = path.join(this.dataDir, "" + routerConf.serverName);
      routerConf.targetUrl = this.targetUrl;
      return MockRouter.build(routerConf, this.proxysConf, this);
    });
  }

  async start(sessionName, mode, offline) {
    await new Promise((resolve, reject) => {
      const serverInstance = this.expressServer.listen(this.proxyPort, () => {
        console.log(
          colors.bgMagenta(
            `Mock server for ${this.routers
              .map(_ => _.serverName)
              .join(",")} running http://localhost:${
              this.proxyPort
            } and forwarding ${this.targetUrl}`
          )
        );
        resolve(serverInstance);
      });
    });
    await Promise.each(this.mockRouters, mockRouter =>
      mockRouter.start(sessionName, mode, offline)
    );
  }

  async stop() {
    // TODO
    this.server.close();
    this.server = null;
    await Promise.each(this.mockRouters, mockRouter => mockRouter.stop());
  }

  logRequest(req, res) {
    this.emit("request", {
      url: req.url,
      params: req.params,
      status: res.statusCode,
      servedBy: res.servedBy,
      modified: req.modified,
      routeId: req.routeId,
      start: req.start,
      stop: req.stop,
      recorded: res.recorded,
      method: req.method,
      serverName: req.serverName
    });
  }

  static async build(conf, proxysConf) {
    const server = new MockServer(conf, proxysConf);
    await server.init();
    return server;
  }
}

module.exports = MockServer;
