const fs = require("fs-extra");
const yaml = require("js-yaml");
const path = require("path");
const _ = require("lodash");
const Promise = require("bluebird");
const SwaggerParser = require("swagger-parser");
const parser = new SwaggerParser();

async function readYml(path) {
  return fs.readFile(path).then(data => yaml.safeLoad(data));
}

async function loadSwaggerDefs(swaggerDefsPath, swaggerDefs = []) {
  const obj = Promise.reduce(
    swaggerDefs,
    async (res, swaggerDefFileName) => {
      const swaggerDefPath = path.resolve(swaggerDefsPath, swaggerDefFileName);
      const swaggerExist = await fs.exists(swaggerDefPath);
      if (!swaggerExist) {
        throw new Error(`Not found : ${swaggerDefPath}`);
      }
      let swaggerObj = await readYml(swaggerDefPath);
      return _.merge(res, swaggerObj);
    },
    {
      swagger: "2.0",
      host: "localhost:9000",
      info: {},
      paths: {}
    }
  );

  return parser.dereference(obj, {
    dereference: {
      circular: false
    },
    validate: {
      spec: true
    }
  });
}

function getHandlersByRouteId(handlers, routeId) {
  if (handlers) {
    const splitIndex = routeId.lastIndexOf("_");
    const pathName = "/" + routeId.substring(0, splitIndex).replace(/_/g, "/");
    const method = routeId.substr(splitIndex + 1).toLowerCase();
    return handlers.paths[pathName] && handlers.paths[pathName][method];
  }
  return null;
}

function getRouteId(req) {
  if (
    req.swagger &&
    req.swagger.operation &&
    req.swagger.operation.operationId
  ) {
    let pathName = req.swagger.pathName;
    if (pathName[0] === "/") {
      pathName = pathName.substr(1);
    }
    const normalizedPath = pathName.replace(/\//g, "_");
    // console.log(req.swagger);
    return `${normalizedPath}_${req.method.toUpperCase()}`;
  } else {
    let pathName = req.path;
    if (pathName[0] === "/") {
      pathName = pathName.substr(1);
    }
    const normalizedPath = pathName.replace(/\//g, "_");
    return `${normalizedPath}_${req.method.toUpperCase()}`;
  }
}

function catchAll(func) {
  return async function(req, res, next) {
    return func(req, res, next).catch(next);
  };
}

module.exports = {
  readYml,
  loadSwaggerDefs,
  getRouteId,
  catchAll,
  getHandlersByRouteId
};
