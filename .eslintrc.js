module.exports = {
  plugins: ["node"],
  extends: [
    "eslint:recommended",
    "plugin:node/recommended",
    "standard",
    "plugin:node/recommended",
    "prettier"
  ],
  rules: {
    "node/exports-style": ["error", "module.exports"]
  }
};
