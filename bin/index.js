#!/usr/bin/env node
const program = require("commander");
const path = require("path");
const _ = require("lodash");
// const fs = require("fs-extra");
const Promise = require("bluebird");
const MockServer = require("../server/MockServer");
// const { readYml } = require("../server/utils");
const colors = require("colors/safe");

let ok = false;

function makeRed(txt) {
  return colors.green(txt);
}

const colorMap = {
  statics: colors.bgCyan.white,
  mock: colors.bgGreen.white,
  proxy: colors.bgBlue.white,
  middleware: colors.bgGreen.white
};

async function startAllServers(sessionName, data, mode, offline) {
  let dataDir = process.cwd();
  if (data) {
    dataDir = path.join(dataDir, data);
  }
  const confPath = path.join(dataDir, "conf");

  let proxysConf;

  try {
    const conf = require(confPath);
    proxysConf = _.map(conf.proxys, (proxyConf, targetUrl) => {
      return {
        ...proxyConf,
        targetUrl,
        dataDir
      };
    });
  } catch (e) {
    console.error(e);
    throw new Error(
      `Could not load configuration from ${confPath}.js, specify data dir with -d option`
    );
  }

  const servers = await Promise.map(proxysConf, proxyConf =>
    MockServer.build(proxyConf, proxysConf)
  );

  servers.forEach(server => {
    server.on("request", data => {
      const colorFn = colorMap[data.servedBy] || colors.bgRed.white;
      console.log(
        colorFn(
          `${data.serverName}[${
            server.proxyPort
          }] ${data.method.toUpperCase()} ${data.url} status:${
            data.status
          } servedBy:${data.servedBy} modified:${!!data.modified} routeId:${
            data.routeId
          }`
        )
      );
    });
  });

  // LAUNCH
  await Promise.mapSeries(servers, async server => {
    await server.start(sessionName, mode, offline);
  });

  console.log(colors.bgMagenta("Mock servers started successfully"));
}

function start(...args) {
  return startAllServers(...args).catch(e => {
    console.log(
      colors.bgRed.white("Mock server failed to start :-( : "),
      colors.bgRed.white(e.message)
    );
    console.error(e);
    process.exit(1);
  });
}

program.description("swagger-mock : proxy, record and transform your APIs");

program.option("-d, --data <path>", "Path to your datas, (current dir)");
program.option("--offline", "Do not try to connect to API servers");

program
  .command("record <sessionName>")
  .description("Start a recording session, will drop existing data")
  .action((sessionName, options) => {
    ok = true;
    start(sessionName, options.parent.data, "record", options.parent.offline);
  });

program
  .command("play [sessionName]")
  .description("Play existing session")
  .action((sessionName, options) => {
    ok = true;

    start(
      sessionName || "default",
      options.parent.data,
      "play",
      options.parent.offline
    );
  });

program
  .command("start")
  .description("Just start proxying")
  .action(options => {
    ok = true;

    start("", options.parent.data, "start", options.parent.offline);
  });

program.parse(process.argv);

if (!ok) {
  program.outputHelp(makeRed);
  process.exit(1);
}
