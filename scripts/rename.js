const _ = require("lodash");
const fs = require("fs-extra");
const path = require("path");
const utils = require("../server/utils");
const Promise = require("bluebird");

const sessionPath = process.argv[2];

if (!sessionPath) {
  throw new Error("path missiing");
}

const handlers = require(path.join(sessionPath, "../../handlers.js"));
// console.log(handlers);
async function exec() {
  const dirs = await fs.readdir(sessionPath);

  Promise.map(dirs, async dir => {
    const stats = await fs.stat(path.join(sessionPath, dir));
    if (stats.isDirectory()) {
      const splitIndex = dir.lastIndexOf("_");
      const uPathName = dir.substring(0, splitIndex);
      const pathName = "/" + uPathName.replace(/_/g, "/");
      //   const operationId = dir.substr(splitIndex + 1).toLowerCase();
      const routeHandler = handlers.paths[pathName];

      if (!routeHandler) {
        throw new Error(`no handler found ${dir} , ${pathName}`);
      }

      const method = routeHandler.get ? "GET" : "POST";

      const newDir = `${uPathName}_${method}`;
      await fs.move(
        path.join(sessionPath, dir),
        path.join(sessionPath, newDir)
      );
    }
  }).catch(e => {
    console.error(e);
  });
}

exec();
