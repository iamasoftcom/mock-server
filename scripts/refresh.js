const rp = require("request-promise-native");
const _ = require("lodash");
const fs = require("fs-extra");
const yaml = require("js-yaml");
const SwaggerParser = require("swagger-parser");
const parser = new SwaggerParser();

const replaceMap = {
  // "/api/dossiers": "/api/dossiers/",
  // "/api/contrats": "/api/contrats/",
  // "/api/integration": "/api/integration/",
  // "/api/iam": "/api/dossiers/"
};

rp({
  uri: "http://localhost:9001/swagger/swagger.json",
  json: true
}).then(async data => {
  // data.paths = _.reduce(
  //   data.paths,
  //   (res, val, path) => {
  //     let newPath = path;
  //     if (!Object.keys(replaceMap).includes(path)) {
  //       Object.keys(replaceMap).forEach(key => {
  //         newPath = newPath.replace(key, replaceMap[key]);
  //       });
  //     }

  //     res[newPath] = val;
  //     return res;
  //   },
  //   {}
  // );
  // data.basePath = "";
  // console.log(data);
  const derefData = await parser.validate(data);
  //   const test = await parser.bundle(data);
  delete data.definitions;
  //   await fs.writeJson("./swagger.json", test, { spaces: 2 });
  await fs.writeFile(
    "./swagger.yml",
    yaml.safeDump(derefData, { noRefs: true })
  );
});
